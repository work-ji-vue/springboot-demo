/*
self.addEventListener('install', function(event) {
	//*
  event.waitUntil(
    caches.open('v1').then(function(cache) {
      return cache.addAll([
		"https://www.photopea.com/?utm_source=homescreen"
		/*
        '/sw-test/',
        '/sw-test/index.html',
        '/sw-test/style.css',
        '/sw-test/app.js',
        '/sw-test/image-list.js',
        '/sw-test/star-wars-logo.jpg',
        '/sw-test/gallery/bountyHunters.jpg',
        '/sw-test/gallery/myLittleVader.jpg',
        '/sw-test/gallery/snowTroopers.jpg'

      ]);
    })
  );
});
*/
self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
          // Cache hit - return response
          if (response) {
            return response;
          }
          return fetch(event.request);
        }
      )
  );
});
//*/
