import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgZorroAntdModule, NZ_I18N, zh_CN} from 'ng-zorro-antd';

import {LoginRegistryRoutingModule} from './login-registry.routing.module';

import {LoginComponent} from './login/login.component';
import {RegistryComponent} from './registry/registry.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    LoginRegistryRoutingModule
  ],
  declarations: [
    LoginComponent,
    RegistryComponent
  ],
  providers: [
    {provide: NZ_I18N, useValue: zh_CN}
  ]
})
export class LoginRegistryModule {
}
