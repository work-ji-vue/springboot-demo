import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../../../auth/auth.guard';

import {BaiduMapComponent} from './baidu-map.component';
import {MapPcComponent} from './pc/map-pc.component';
import {MapMobileComponent} from './mobile/map-mobile.component';

const routes: Routes = [
  {path: '', redirectTo: 'mobile', pathMatch: 'full', canActivate: [AuthGuard]},
  {
    path: '', component: BaiduMapComponent, children: [
      {path: 'pc', component: MapPcComponent, canActivate: [AuthGuard]},
      {path: 'mobile', component: MapMobileComponent, canActivate: [AuthGuard]},
    ], canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class BaiduMapRoutingModule {
}
