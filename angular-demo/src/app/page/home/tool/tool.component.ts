import {Component, OnInit} from '@angular/core';
import {SharedService} from '../../../shared/shared.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-tool',
  templateUrl: './tool.component.html',
  styleUrls: ['./tool.component.css']
})
export class ToolComponent implements OnInit {

  contentMinHeight;

  constructor(private sharedService: SharedService) {
    sharedService.sub('PageContentMinHeight').subscribe(res => {
      this.contentMinHeight = (res.msg - 41 - 69) + 'px';
    });
  }

  ngOnInit() {
  }

}
