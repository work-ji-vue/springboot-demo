import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IcoGeneratorComponent } from './ico-generator.component';

describe('ToolComponent', () => {
  let component: IcoGeneratorComponent;
  let fixture: ComponentFixture<IcoGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IcoGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IcoGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
