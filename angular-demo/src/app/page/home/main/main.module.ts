import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgZorroAntdModule, NZ_I18N, zh_CN} from 'ng-zorro-antd';

import {MainRoutingModule} from './main-routing.module';

import {MainComponent} from './main.component';

@NgModule({
  imports: [
    CommonModule,
    NgZorroAntdModule,
    MainRoutingModule
  ],
  declarations: [
    MainComponent
  ]
})
export class MainModule {
}
