import {Injectable} from '@angular/core';
import {HttpClientService} from '../../../http/http-client.service';

@Injectable()
export class MainService {

  constructor(private http: HttpClientService) {
  }

  test() {
    return this.http.request({
      method: 'GET',
      url: '/test/hello',
      data: null
    });
  }

}
