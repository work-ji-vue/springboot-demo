import {Component, OnInit} from '@angular/core';

import {MainService} from './main.service';

@Component({
  selector: 'app-home-test',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  providers: [MainService]
})
export class MainComponent implements OnInit {

  constructor(private mainService: MainService) {
  }

  ngOnInit() {
  }

  sendTest(): void {
    this.mainService.test().then(res => {
      console.log(res);
    }).catch(err => {
      console.error(err);
    });
  }

}
