import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../../auth/auth.guard';

import {HomeComponent} from './home.component';

const routes: Routes = [
  {path: '', redirectTo: 'main', pathMatch: 'full', canActivate: [AuthGuard]},
  {
    path: '', component: HomeComponent, children: [
      {path: 'main', loadChildren: './main/main.module#MainModule', canActivate: [AuthGuard]},
      {path: 'baidu-map', loadChildren: './baidu-map/baidu-map.module#BaiduMapModule', canActivate: [AuthGuard]},
      {path: 'tool', loadChildren: './tool/tool.module#ToolModule', canActivate: [AuthGuard]},
      {path: 'tech', loadChildren: './tech/tech.module#TechModule', canActivate: [AuthGuard]},
    ], canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class HomeRoutingModule {
}
