import {Injectable} from '@angular/core';
import {HttpClientService} from '../http/http-client.service';
import {UserVo} from '../entity/vo/user.vo';

@Injectable()
export class AuthService {

  constructor(private http: HttpClientService) {
  }

  login(user: UserVo) {
    return this.http.request({
      method: 'POST',
      url: '/auth/login',
      data: user
    }).then(res => {
      if (res.retcode === 1) {
        localStorage.setItem('login_info', JSON.stringify({
          token: res.data['token'],
          userInfo: res.data['user']
        }));
      }
      return res;
    });
  }

  logout() {
    localStorage.removeItem('login_info');
  }

  public get loggedIn(): boolean {
    return (localStorage.getItem('login_info') !== null);
  }

}
