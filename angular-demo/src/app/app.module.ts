import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientService} from './http/http-client.service';
import {HttpInterceptorProvider} from './http/http-interceptor.provider';
import {AuthService} from './auth/auth.service';
import {AuthGuard} from './auth/auth.guard';
import {SharedService} from './shared/shared.service';
import {NgZorroAntdModule, NZ_ICONS, NZ_I18N, zh_CN} from 'ng-zorro-antd';
import {IconDefinition} from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';
import {registerLocaleData} from '@angular/common';
import zh from '@angular/common/locales/zh';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {LoginRegistryModule} from './page/login-registry/login-registry.module';

registerLocaleData(zh);

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key]);

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgZorroAntdModule,
    LoginRegistryModule
  ],
  bootstrap: [AppComponent],
  providers: [
    HttpClientService,
    AuthService,
    AuthGuard,
    SharedService,
    HttpInterceptorProvider,
    {provide: NZ_ICONS, useValue: icons},
    {provide: NZ_I18N, useValue: zh_CN}
  ]
})

export class AppModule {
}

export function tokenGetter() {
  return localStorage.getItem('access_token');
}
