import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class SharedService {

  private subjects = new Map<string, Subject<any>>();

  /* 订阅 */
  sub(topic: string) {
    if (this.subjects.get(topic) == null) {
      this.subjects.set(topic, new Subject<any>());
    }
    return this.subjects.get(topic).asObservable();
  }

  /* 发布 */
  pub(topic: string, msg: any): void {
    if (this.subjects.get(topic) == null) {
      this.subjects.set(topic, new Subject<any>());
    }
    this.subjects.get(topic).next({topic: topic, msg: msg});
  }

}

