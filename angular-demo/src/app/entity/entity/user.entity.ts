export class UserEntity {

  username: string;
  password: string;
  roles: string;
  nickname: string;
  tel: string;

  constructor() {
  }

}
