# 基于Angular与SpringBoot的前后端分离项目后台模板

#### 项目介绍
1. 前端使用Angular6框架，UI库使用ng-zorro
2. 后端使用SpringBoot2框架，使用SpringSecurity进行权限管理，使用Token进行登录验证
3. 该项目数据库使用SQL Server，若需修改为其它数据库只需更改依赖包以及配置文件即可
4. 在原网络教程的基础上删减了难懂复杂的部分，整合了自己的理解，简化了配置文件，拒绝无脑复制粘贴。本着开箱即用的原则，导入IDE，依赖包下载完成后即可运行
5. 项目预览地址：http://angularme.cn:4200

#### 开发环境
1. 开发工具：前端：WebStorme，后端：Eclipse
2. 环境node.js10，angular-cli（需安装Python27），jdk8
3. 项目编码为UTF-8

#### 注意事项
1. 前端在build后使用nginx部署即可，不建议将其放在后台项目中，避免打包部署上的麻烦以及前后端路由的混乱
2. 在npm install 时遇见 MSBUILD : error MSB3428: 未能加载 Visual C++ 组件“VCBuild.exe时，执行 npm install --global --production windows-build-tools 即可
3. 开发和部署时应注意跨域相关设置，详细请见服务端nginx、前端代码中代理设置（这两个文件无需改动）、以及http-interceptor.service.ts（basePath开发时需改为''，build时需改为'/api'）
4. 后端项目已对外部Tomcat容器进行相关配置，可以将其放入Tomcat中，也可以单独运行（Tomcat配置相关代码已注释），同时也为打包jar做相关配置

#### 参考教程
##### 前端
1. HTTP封装及拦截器配置 https://angular.cn/guide/http
2. 路由懒加载模块 https://blog.csdn.net/qq_41648452/article/details/82863618
3. token存储 https://www.jianshu.com/p/a9a535edf79f
4. 网页版ps来自https://www.photopea.com/
##### 后端
1. https://www.cnblogs.com/hackyo/p/8004928.html
2. https://www.jianshu.com/p/6307c89fe3fa