package com.example.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.entity.entity.UserEntity;

@Mapper
public interface UserDao {

	@Select("SELECT username,password,roles FROM userinfo WHERE username=#{username}")
	UserEntity findByUsername(@Param("username") String username);

	@Select("SELECT username,roles,nickname,tel FROM userinfo WHERE username=#{username}")
	UserEntity getUser(@Param("username") String username);

	@Insert("INSERT INTO userinfo (username,password,roles,nickname,tel) VALUES (#{user.username},#{user.password},#{user.roles},#{user.nickname},#{user.tel})")
	Integer insert(@Param("user") UserEntity user);

}
