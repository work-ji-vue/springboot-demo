package com.example.redis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.redis.dao.RedisLoginDao;

@Service
public class RedisLoginService {

	@Autowired
	private RedisLoginDao redisLoginDao;

	public void login(String username, String token) {
		synchronized (this) {
			redisLoginDao.setLoginToken(username, token);
		}
	}

	public Boolean validate(String username, String token) {
		synchronized (this) {
			return token.equals(redisLoginDao.getLoginToken(username));
		}
	}

}
