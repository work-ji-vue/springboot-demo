package com.example.redis.dao;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.example.redis.dao.base.RedisGeneratorDao;

@Repository
public class RedisLoginDao extends RedisGeneratorDao<String, String> {

	@Value("${jwt.loginCache.pre}")
	private String tokenHead;

	@Value("${jwt.loginCache.timeout}")
	private int timeout;

	public void setLoginToken(String username, String token) {
		add(tokenHead + username, token, timeout, TimeUnit.MINUTES);
	}

	public String getLoginToken(String username) {
		return get(tokenHead + username);
	}

}
