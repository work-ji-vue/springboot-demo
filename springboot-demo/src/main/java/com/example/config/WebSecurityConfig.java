package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.example.handler.EntryPointUnauthorizedHandler;
import com.example.handler.RestAccessDeniedHandler;
import com.example.security.JwtAuthenticationTokenFilter;
import com.example.security.JwtUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtUserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

	@Autowired
	private EntryPointUnauthorizedHandler entryPointUnauthorizedHandler;

	@Autowired
	private RestAccessDeniedHandler restAccessDeniedHandler;

	@Value("${jwt.route.authentication.path}")
	private String authPath;

	@Autowired
	public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(this.jwtUserDetailsService)
				.passwordEncoder(passwordEncoderBean());
	}

	@Bean
	public PasswordEncoder passwordEncoderBean() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
				// 禁用csrf
				.csrf().disable()
				// 关闭session
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				//
				.authorizeRequests()
				//
				.antMatchers(HttpMethod.GET, "/**").permitAll()
				// 对于获取token的rest api要允许匿名访问
				.antMatchers("/" + authPath + "/**").permitAll()
				//
				.anyRequest().authenticated().and().headers().cacheControl();
		httpSecurity.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
		httpSecurity.exceptionHandling().authenticationEntryPoint(entryPointUnauthorizedHandler)
				.accessDeniedHandler(restAccessDeniedHandler);

	}

}
