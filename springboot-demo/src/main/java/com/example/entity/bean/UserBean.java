package com.example.entity.bean;

import java.util.Arrays;
import java.util.List;

import com.example.entity.entity.UserEntity;

import lombok.Data;

@Data
public class UserBean {

	private Integer id;
	private String username;
	private String password;
	private List<String> roles;
	private String nickname;
	private String tel;

	public UserBean() {

	}

	public UserBean(UserEntity userEntity) {
		super();
		this.id = userEntity.getId();
		this.username = userEntity.getUsername();
		this.password = userEntity.getPassword();
		if (userEntity.getRoles() != null && !"".equals(userEntity.getRoles())) {
			this.roles = Arrays.asList(userEntity.getRoles().split(","));
		}
		this.nickname = userEntity.getNickname();
		this.tel = userEntity.getTel();
	}

	public UserBean(Integer id, String username, String password, List<String> roles, String nickname, String tel) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.roles = roles;
		this.nickname = nickname;
		this.tel = nickname;
	}

}
