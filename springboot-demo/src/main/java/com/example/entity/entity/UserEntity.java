package com.example.entity.entity;

import com.example.entity.bean.UserBean;

import lombok.Data;

@Data
public class UserEntity {

	private Integer id;
	private String username;
	private String password;
	private String roles;
	private String nickname;
	private String tel;

	public UserEntity() {
	}

	public UserEntity(UserBean userBean) {
		super();
		this.id = userBean.getId();
		this.username = userBean.getUsername();
		this.password = userBean.getPassword();
		StringBuffer rolesSb = new StringBuffer();
		if (userBean.getRoles() != null && userBean.getRoles().size() > 0) {
			for (String role : userBean.getRoles()) {
				rolesSb.append(role + ",");
			}
		}
		String roles = rolesSb.toString();
		this.roles = roles.substring(0, roles.length() - 1);
		this.nickname = userBean.getNickname();
		this.tel = userBean.getTel();
	}

	public UserEntity(Integer id, String username, String password, String roles, String nickname, String tel) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.roles = roles;
		this.nickname = nickname;
		this.tel = nickname;
	}

}
